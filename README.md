 - This is a sample django project created using Django-1.3.1 and it includes basic chat using jQuery, Ajax and django.
 - `requirements.pip` included for project setup

### Browser 1

Login with username `s` and password `s`. Then go the URL `http://localhost:8000/users`.

### Browser 2

Login with username `a` and password `a`. Then go the URL `http://localhost:8000/users`.

Now the chat will work between these two users.

### db.sh

The `db.sh` shell script file can be used to create drop and create a fresh sqlite database.
