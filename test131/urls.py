from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'test131.views.home', name='home'),
    # url(r'^test131/', include('test131.foo.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    (r'^$', 'django.views.generic.simple.redirect_to', {'url': '/admin'}),

    url(r'^users/', 'test131.myapp.views.logged'),
    url(r'^online_users/', 'test131.myapp.views.online_users'),
    url(r'^activity/', 'test131.myapp.views.last_activity'),
    url(r'^configplan/$','test131.myapp.views.configplan',name="config_plan"),
)
