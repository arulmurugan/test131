(function($){
	$(function(){
		$(document).ready(function() {
			$('#id_action').bind('change', type_change);
			$('#id_data >option').show();
		});
	});
})(django.jQuery);

// based on the type, action will be loaded

var $ = django.jQuery.noConflict();

function type_change()
{
	var action_type = $('#id_action').val();
	$.ajax({
		"type" : "GET",
		"url"  : "/configplan/?action_type="+action_type,
	"dataType" : "json",
	"cache"    : false,
	"success"  : function(json) {
			$('#id_data >option').remove();
			for(var j = 0; j < json.length; j++){
				$('#id_data').append($('<option></option>').val(json[j][0]).html(json[j][1]));
			}
		}
	})(jQuery);
}
