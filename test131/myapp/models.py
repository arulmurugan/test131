import datetime

from django.db import models
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.auth.models import User

from test131.util.models import Audit

ACTION_CHOICES = (
                ("queue","queue"),
                ("play","play"),
                ("transfer","transfer"),
                ) 

class SampleModel(Audit):
    text   = models.CharField(max_length = 100)
    action = models.CharField(verbose_name="Action",max_length=45, choices=ACTION_CHOICES)
    data = models.CharField(verbose_name="Data",max_length=45,choices = (('', ''), ))

    def __unicode__(self):
        return self.text

class LastActivity(Audit):
    time = models.DateTimeField(default = datetime.datetime.now())
    user = models.ForeignKey(User, related_name = 'current_user')

    def __unicode__(self):
        return str(self.time.strftime('%I:%M:%S %p'))

class Messages(Audit):
    sender   = models.ForeignKey(User, related_name = 'sender_user')
    receiver = models.ForeignKey(User, related_name = 'receiver_user')
    closed   = models.BooleanField(default = False)

class LoggedUser(models.Model):
    username   = models.CharField(max_length = 30)
    session_id = models.CharField(max_length = 250, primary_key=True)
  
    def __unicode__(self):
        return self.username

def login_user(sender, request, user, **kwargs):
    LoggedUser(username   = user.username,
               session_id = request.session.session_key,
               ).save()

def logout_user(sender, request, user, **kwargs):
    try:
        u = LoggedUser.objects.get(username = user.username,
                                   pk       = request.session.session_key,
                                   )
        u.delete()
    except LoggedUser.DoesNotExist:
        pass

user_logged_in.connect(login_user)
user_logged_out.connect(logout_user)
