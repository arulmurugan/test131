from django.contrib import admin

from test131.myapp.models import SampleModel
from test131.util.admin import AuditAdmin

class SampleModelAdmin(AuditAdmin):
    list_display         = ('text', ) + AuditAdmin.list_display

    class Media:
        js = ['/static/js/action_change.js']

admin.site.register(SampleModel, SampleModelAdmin)
