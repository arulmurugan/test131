import datetime

from django.shortcuts import render_to_response, RequestContext, HttpResponse
from django.utils import simplejson

from test131.myapp.models import LoggedUser, SampleModel, LastActivity

def logged(request):
    logged_users = LoggedUser.objects.values('username').distinct('username').order_by('username')
    text         = SampleModel.objects.all().order_by('created_at')

    if request.is_ajax():
        if request.POST:
            print "AAAAAAAAAAAAAAAAA", request.POST
            SampleModel(text       = request.POST['chat_text'],
                        created_by = request.user,
                        created_at = datetime.datetime.now(),
                        ).save()

        text = SampleModel.objects.all().order_by('created_at')
        chat_list = []
        for each in text:
            chat_str  = "<b>%s</b>: %s" % (each.created_by, each.text)
            chat_dict = {
                        "id": each.pk,
                        "value": chat_str,
                        }
            chat_list.append(chat_dict)
        json = simplejson.dumps(chat_list)
        return HttpResponse(json, mimetype='application/javascript')

    return render_to_response('index1.html', locals(), context_instance=RequestContext(request))

def last_activity(request):
    if LastActivity.objects.filter(user = request.user):
        LastActivity.objects.filter(user = request.user).update(time = datetime.datetime.now())
    else:
        LastActivity(time = datetime.datetime.now(),
                     user = request.user,
                     ).save()
    time = LastActivity.objects.get(user = request.user).time
    json = simplejson.dumps(str(time))
    return HttpResponse(json, mimetype='application/javascript')

def online_users(request):
    users = LastActivity.objects.filter(time__gte = datetime.datetime.now() - datetime.timedelta(seconds = 6)).exclude(user = request.user)
    users_list = []
    for each in users:
        users_dict = {
                    "user": str(each.user),
                    }
        users_list.append(users_dict)
    print "users_list::: ", users_list
    json = simplejson.dumps(users_list)
    return HttpResponse(json, mimetype='application/javascript')

def action_choices(request): 
    action_list = []
    ChoiceA = ("on-false", "on-true")
    ChoiceB = ("always", "never")

    action_type = request.GET.get('action_type')
    print "type:::::::: ", action_type
    if str(action_type).lower() == 'a':
        choices = ChoiceA
    elif str(action_type).lower() == 'b':
        choices = ChoiceB
    else:
        choices = ()

    [action_list.append((each,each)) for each in choices]
    json = simplejson.dumps(action_list)
    return HttpResponse(json, mimetype='application/javascript')

def configplan(request):
    action_list = []

    queue = ("support", "sales")
    play = ("file1", "file2")
    transfer = ("200","300")
    fac = ("agent_login","agent_logout")
    
    action_type = request.GET.get('action_type')
    print "action_type::: ", action_type

    if str(action_type).lower() == 'queue':
        choices = queue
    elif str(action_type).lower() == 'play':
        choices = play
    elif str(action_type).lower() == 'transfer':
        choices = transfer
    elif str(action_type).lower() == 'fac':
        choices = fac
    else:
        choices = ()
    
    [action_list.append((each,each)) for each in choices]
    json = simplejson.dumps(action_list)
    return HttpResponse(json, mimetype='application/javascript')
