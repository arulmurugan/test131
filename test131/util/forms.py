import re

from django.forms.util import ErrorList

from django.forms import BaseModelForm
from django.forms.models import ModelFormMetaclass, model_to_dict

class CustomModelForm(BaseModelForm):
    __metaclass__ = ModelFormMetaclass
    
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix='',
                 empty_permitted=False, instance=None):
        opts = self._meta
        if instance is None:
            if opts.model is None:
                raise ValueError('ModelForm has no model class specified.')
            # if we didn't get an instance, instantiate a new one
            self.instance = opts.model()
            object_data = {}
        else:
            self.instance = instance
            object_data = model_to_dict(instance, opts.fields, opts.exclude)
        # if initial was provided, it should override the values from instance
        if initial is not None:
            object_data.update(initial)
        # self._validate_unique will be set to True by BaseModelForm.clean().
        # It is False by default so overriding self.clean() and failing to call
        # super will stop validate_unique from being called.
        self._validate_unique = False
        label_suffix = "?"
        super(CustomModelForm, self).__init__(data, files, auto_id, prefix, object_data,
                                            error_class, label_suffix, empty_permitted)

def mobile_number_validation(mobile_number_original):
    mobile_number = str(mobile_number_original).replace(' ', '').replace('+', '').replace('-', '')   
    if mobile_number != '':
        while(mobile_number.startswith('0')): # To remove the leading zero's
            mobile_number = mobile_number[1:len(mobile_number)]
    
        if not (re.compile('^[1-9][0-9]*$')).match(mobile_number) or (len(mobile_number) > 10 
                    and not mobile_number.startswith('91'))  or  (len(mobile_number) > 12 or len(mobile_number) < 10) or (len(mobile_number) == 11):
            return False
    
        if len(mobile_number) == 10:
            mobile_number = '91' + mobile_number
    return mobile_number